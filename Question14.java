import java.util.Scanner;

public class Question14 {

	public static void main(String[] args) {
		int number;
		Scanner scan = new Scanner(System.in);
		System.out.println("Input a postive integer: ");
		number = scan.nextInt();
		
		System.out.println("The multiplication table of "+number+" is:");
		
		for ( int i = 0; i<10; i++ ) {
			int multiply = number*(i+1);
			
			System.out.println(number+"X"+(i+1)+"= "+multiply);
		}
		
	}

}
