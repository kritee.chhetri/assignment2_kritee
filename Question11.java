import java.util.Scanner;

public class Question11 {

	public static void main(String[] args) {
		float revenue = 0;
		float discountAmt = 0;
		float rate = 0;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter unit price: ");
		float unit = scan.nextInt();
		System.out.println("Enter quantity: ");
		int quantity = scan.nextInt();
		
		if(quantity<100) {
			 revenue = unit*quantity;
		}
			else if(quantity>=100 && quantity<=120){
				  rate = 10/100;
				  revenue = unit*quantity;
				  discountAmt = revenue*rate;
				 revenue -=discountAmt;
			}
			else if (quantity>120) {
				 rate = 15/100;
				 revenue = unit*quantity;
				 discountAmt = revenue*rate;
				revenue -=discountAmt;
		}
		
		System.out.println("The revenue from sale: "+revenue+"$");
		System.out.println("After discount: "+discountAmt+"$("+rate*100+"%)");

	}

}
