import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Input first number: ");
		int firstNumber = scan.nextInt();
		
		System.out.println("Input second number: ");
		int secondNumber = scan.nextInt();
		
		System.out.println("Input third number: ");
		int thirdNumber = scan.nextInt();
		
		System.out.println("Input fourth number: ");
		int fourthNumber = scan.nextInt();

		if (firstNumber!=secondNumber&&secondNumber!=thirdNumber&&thirdNumber!=fourthNumber&&fourthNumber!=firstNumber) {
			System.out.println("Numbers are not equal!");
		}
		else
			System.out.println("One of the numbers are equal");

	}
}
