import java.util.Scanner;

public class Question10 {
	public int scores(int quiz, int midTerm, int finalScores) {
		int average = (quiz+midTerm+finalScores)/3;
		return average;
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Quiz Score: ");
		int quiz = scan.nextInt();
		
		System.out.println("Mid-term Score: ");
		int midTerm = scan.nextInt();
		
		System.out.println("Final Score: ");
		int finalScores = scan.nextInt();
		
		Question10 score= new Question10();
		if(score.scores(quiz, midTerm, finalScores)>=90) {
			System.out.println("Your grade is A");
		}
		if(score.scores(quiz, midTerm, finalScores)>=70 && score.scores(quiz, midTerm, finalScores)<=90 ) {
			System.out.println("Your grade is B");
		}
		if(score.scores(quiz, midTerm, finalScores)>=50 && score.scores(quiz, midTerm, finalScores)<70 ) {
			System.out.println("Your grade is C");
		}
		if(score.scores(quiz, midTerm, finalScores)<50) {
			System.out.println("Your grade is F");
		}
	}

}
