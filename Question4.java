import java.util.Scanner;

public class Question4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Input number of months: ");
		int month = scan.nextInt();
		double borrow = 100000;
		for(int i=0; i<month; i++) {
			borrow = (double) (borrow+borrow*0.04);
			if(borrow%1000 !=0) {
				borrow-= borrow%1000;
				borrow+=1000;
			}
			
		}
		System.out.println("Amount of debt: ");
		System.out.printf("%.0f\n",borrow);
	}

}
