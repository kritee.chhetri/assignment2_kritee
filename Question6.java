import java.util.Scanner;

public class Question6 {

	int findPerimeter(int length, int width) {
		int perimeter = 2*(length+width);
		return perimeter;
	}
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter length: ");
		int length = scan.nextInt();
		
		System.out.println("Enter width: ");
		int width = scan.nextInt();
		
		Question6 question = new Question6();
		System.out.println(question.findPerimeter(length, width));
		

	}

}
