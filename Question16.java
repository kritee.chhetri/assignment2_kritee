import java.util.Scanner;

public class Question16 {

	public static void main(String[] args) {
		
		char yesno;
		Scanner scan = new Scanner(System.in);
		
		do {
			System.out.println("Enter first number: ");
			int number1 = scan.nextInt();
			
			System.out.println("Enter second number: ");
			int number2 = scan.nextInt();
			
			 int sum = number1+number2;
			 System.out.println("sum is: "+sum);
			 
			 System.out.println("Do you wish to perform the operation again(y/n)?");
			  yesno = scan.next().charAt(0);
			  
			  System.out.println();
			 
		
		}while( yesno == 'y' ||  yesno == 'y');
		
	
	}

}
