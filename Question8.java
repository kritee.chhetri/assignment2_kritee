
public class Question8 {


	public static void main(String[] args) {
		
		String s1 = "Hello World";
		StringBuilder newS1 = new StringBuilder();
		
		String s2 = "The quick brown fox.";
		StringBuilder newS2 = new StringBuilder();
		
		String s3 = "Edibit is really helpful!";
		StringBuilder newS3 = new StringBuilder();
		
		newS1.append(s1);
		newS1 = newS1.reverse();
		System.out.println(newS1);
		
		newS2.append(s2);
		newS2 = newS2.reverse();
		System.out.println(newS2);
		
		newS3.append(s3);
		newS3 = newS3.reverse();
		System.out.println(newS3);
		
		
	
	}

}
